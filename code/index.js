/* Создать объект "Документ", в котором определить свойства "Заголовок, тело, футер, дата". Создать в объекте 
вложенный объект - "Приложение". Создать в объекте "Приложение", вложенные объекты, "Заголовок, тело, футер, дата". 
Создать методы для заполнения и отображения документа. */

let doc = {
    title: "",
    body: "",
    footer: "",
    data: "",
    add: {
        title: "",
        body: "",
        footer: "",
        data: ""
    },

    inputData: function () {
        this.title = prompt("Введите заголовок документа");
        this.body = prompt("Введите тело документа");
        this.footer = prompt("Введите футер документа");
        this.data = prompt("Введите дату документа");
        this.add.title = prompt("Введите заголовок приложения документа");
        this.add.body = prompt("Введите тело приложения документа");
        this.add.footer = prompt("Введите футер приложения документа");
        this.add.data = prompt("Введите дату приложения документа");
    },

    showData: function () {
        document.write("Your doc title: " + this.title + "<br>");
        document.write("Your doc body: " + this.body + "<br>");
        document.write("Your doc footer: " + this.footer + "<br>");
        document.write("Your doc data: " + this.data + "<br>");
        document.write("Your add title: " + this.add.title + "<br>");
        document.write("Your add body: " + this.add.body + "<br>");
        document.write("Your add footer: " + this.add.footer + "<br>");
        document.write("Your add data: " + this.add.data + "<br>");

    }
};

doc.inputData();
doc.showData();